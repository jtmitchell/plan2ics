# plan2ics

Script to read a calendar file used by netplan and convert it to ICS format
suitable for import into Google Calendar.

[plan](http://www.bitrot.de/plan.html) is a X/Motif calendar application written by Thomas Driemeyer

# Installing the script with pip

```
pip install plan2ics --index-url https://<personal_access_token_name>:<personal_access_token>@gitlab.com/api/v4/projects/14807046/packages/pypi/simple
```

# Development Install

Install [uv](https://docs.astral.sh/uv/), then set up the virtual environment.

```
curl -LsSf https://astral.sh/uv/install.sh | sh
uv sync
```

## *just* a tool to automate the builds

Install the tool [just](https://github.com/casey/just) as a handy alternative to ``make``

## Pre-commit Git Hooks

The project is using ``pre-commit`` to do some checking when commiting.

* install the [pre-commit hooks for git](https://pre-commit.com/)
```
pre-commit install --hook-type pre-commit --hook-type commit-msg
```

The pre-commit hooks are configured to do the following:
* format and lint the code using [**ruff**](https://docs.astral.sh/ruff/)
* check code for security issues
* fix trailing whitespace
* validate yaml files
* check we are not adding huge files to the repo
* add a ticket id (from the branch) to the commit message

The ``pre-commit`` checks will run when you commit code,
and may stop you committing the code until the checks are passing.

* To run the checks on the changed files, before the commit, run ``pre-commit run``
* To run the checks on all the files, run ``pre-commit run --all-files``

The ``pre-commit-update`` command is available to update the versiosns of the pre-commit tools.
