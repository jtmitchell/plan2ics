"""
Class data models.
"""

# ruff: noqa: D101, D102, D103, S324, DTZ007, D107, S108

import datetime
import hashlib
import re
import uuid

import pytz
import tzlocal
import vobject
from dateutil.rrule import rruleset, rrulestr

datetime_rx = re.compile(r"(?P<date>\d+/\d+/\d+)\s+(?P<time>\d+:\d+:\d+)")
duration_rx = re.compile(r"\s+(?P<hours>\d+):(?P<minutes>\d+):(?P<seconds>\d+)")
exception_rx = re.compile(r"E\s+(?P<date>\d+/\d+/\d+)")
repeat_rx = re.compile(
    r"R\s+(?P<trigger_secs>\d+)\s+(?P<delete_secs>\d+)\s+(?P<weekdaymap>\d+)"
    r"\s+(?P<monthdaymap>\d+)\s+(?P<yearly>\d)"
)
note_rx = re.compile(r"N\s+(?P<message>.*)$")
message_rx = re.compile(r"M\s+(?P<message>.*)$")
where_rx = re.compile(r"Where\s*:\s*(?P<location>\w.*)$")
script_rx = re.compile(
    r"S\s+#plan2ics:\s+version=(?P<version>\d+)\s+uuid=(?P<uid>[\w-]+)\s+hash=(?P<hash>\w+)$"
)
groupmtg_rx = re.compile(r"G\s+")

epoch = datetime.date(1970, 1, 1)
weekday = (
    "SU",
    "MO",
    "TU",
    "WE",
    "TH",
    "FR",
    "SA",
)
weeknumber = (
    "1",
    "2",
    "3",
    "4",
    "5",
    "-1",
)
one_day = datetime.timedelta(days=1)
translate_map = str.maketrans("\xa0", " ")


class Event:
    _uid = None
    vevent = None  # ICS version of event
    pevent = None  # array containing netplan version of event
    extra = None
    verbose = False

    def __init__(self, vevent, event, verbose=False):
        self.vevent = vevent
        self.pevent = event
        self.verbose = verbose
        self.extra = []
        self._load_plan()

    @property
    def uid(self):
        return str(self._uid)

    @property
    def hash(self):
        return hashlib.md5(self.pevent[0].encode("utf8")).hexdigest()  # nosec

    @property
    def plan(self):
        if self.pevent:
            pevents = "".join(self.pevent)
            extras = "\n".join(self.extra)
            return f"{pevents}{extras}"
        return ""

    @property
    def ics(self):
        if self.vevent:
            return self.vevent
        return ""

    def _escape(self, text):
        """
        Return the given text with everything above ASCII 128 removed.
        """
        text = "".join([x for x in text if ord(x) < 128])
        return text

    def _load_plan(self):  # noqa
        dt = datetime_rx.match(self.pevent[0])
        dt_start = None
        dt_end = None
        dt_until = None
        time = dt.group("time")
        if time == "99:99:99":
            # there is no alarm trigger time
            # I will treat these as transparent, all-day events
            dt_start = datetime.datetime.strptime(f"{dt.group('date')}", r"%m/%d/%Y").date()
            self.vevent.add("dtstart").value = dt_start
            dt_end = dt_start + one_day
            self.vevent.add("transp").value = "TRANSPARENT"
        else:
            # we have a trigger time, and will use it for the end time
            # until something better comes along
            dt_start = datetime.datetime.strptime(
                f"{dt.group('date')} {time}", r"%m/%d/%Y %H:%M:%S"
            )
            self.vevent.add("dtstart").value = dt_start
            self.vevent.add("transp").value = "OPAQUE"
        description = []
        rrule_set = None
        location = None
        for line in re.split(r"\n", self.pevent[1]):
            if not line:
                continue
            if line[0] == "N":
                m = note_rx.match(line)
                if m:
                    self.vevent.add("summary").value = self._escape(m.group("message"))
                    if "@" in m.group("message"):
                        location = m.group("message").split("@", 1)[-1]
            elif line[0] == "M":
                m = message_rx.match(line)
                if m:
                    description.append(self._escape(m.group("message")))
                    match = where_rx.match(m.group("message"))
                    if match:
                        location = match.group("location")
            elif line[0] == "R":
                m = repeat_rx.match(line)
                if m:
                    rrlist = []
                    repeat_days = None
                    if not rrule_set:
                        rrule_set = rruleset()
                    if not m.group("delete_secs") == "0":
                        dt_until = epoch + datetime.timedelta(
                            seconds=int(m.group("delete_secs"))
                        )
                        rrlist.append(f"UNTIL={dt_until}")
                    if not m.group("trigger_secs") == "0":
                        repeat_days = int(m.group("trigger_secs")) / 86400
                        rrlist.append(f"INTERVAL={int(repeat_days)}")
                    if m.group("yearly") == "1":
                        rrlist.append("FREQ=YEARLY")
                    elif not m.group("monthdaymap") == "0":
                        rrlist.append("FREQ=MONTHLY")
                        monthdaymap = int(m.group("monthdaymap"))
                        daylist = []

                        for i in range(1, 31):
                            if monthdaymap & (1 << i):
                                daylist.append(str(i))

                        # bit 0 is set, so it is the last day of the month
                        if monthdaymap & 1:
                            daylist.append("-1")
                        if daylist:
                            rrlist.append(f"BYMONTHDAY={','.join(daylist)}")

                    elif not m.group("weekdaymap") == "0":
                        days = []
                        weeks = []
                        weekdaymap = int(m.group("weekdaymap"))
                        for i in range(7):
                            if weekdaymap & (1 << i):
                                days.append(weekday[i])
                        for i in range(8, 14):
                            if weekdaymap & (1 << i):
                                weeks.append(weeknumber[i - 8])
                        if weeks:
                            rrlist.append("FREQ=MONTHLY")
                            if days:
                                rrlist.append(f"BYDAY={','.join(days)}")
                            if weeks:
                                rrlist.append(f"BYSETPOS={','.join(weeks)}")
                        else:
                            rrlist.append("FREQ=WEEKLY")
                            if days:
                                rrlist.append(f"BYDAY={','.join(days)}")
                    else:
                        rrlist.append("FREQ=DAILY")
                    if self.verbose:
                        print(f"days {''} rrlist {';'.join(rrlist)}")
                    rrule_set.rrule(rrulestr(";".join(rrlist)))
            elif line[0] == "E":
                m = exception_rx.match(line)
                if m:
                    if not rrule_set:
                        rrule_set = rruleset()
                    rrule_set.exdate(
                        datetime.datetime.strptime(f"{m.group('date')}", r"%m/%d/%Y")
                    )
            elif line[0] == "S":
                s = script_rx.match(line)
                if s:
                    self._uid = s.group("uid")
            elif line[0] == "G":
                continue
            else:
                m = duration_rx.match(line)
                if m:
                    duration = datetime.timedelta(
                        hours=int(m.group("hours")),
                        minutes=int(m.group("minutes")),
                        seconds=int(m.group("seconds")),
                    )
                    if duration:
                        dt_end = dt_start + duration
        if location:
            self.vevent.add("location").value = location
        if dt_end:
            self.vevent.add("dtend").value = dt_end
        else:
            self.vevent.add("dtend").value = dt_start
        if description:
            self.vevent.add("description").value = " ".join(description)
        if rrule_set:
            if self.verbose:
                print(f"plan event {self.plan}")
            self.vevent.rruleset = rrule_set
        if not self._uid:
            # generate a UID and save it in the netplan data
            self._uid = uuid.uuid3(uuid.NAMESPACE_OID, f"{dt_start} {' '.join(description)}")
            self.extra.append(f"S\t#plan2ics: version={0} uuid={self.uid} hash={self.hash}\n")


class Dayplan:
    calendar = None
    timezone = None
    events = None
    verbose = False

    def __init__(self, infile=None, date_threshold_delta=None, verbose=False):
        self.events = []
        self.calendar = vobject.iCalendar()
        self.timezone = pytz.timezone(tzlocal.get_localzone_name())
        tz = self.calendar.add("vtimezone")
        tz.settzinfo(self.timezone)
        self.verbose = verbose
        self.date_threshold_delta = date_threshold_delta
        if date_threshold_delta:
            self.date_threshold = datetime.datetime.now() - date_threshold_delta
        if infile:
            self._load(infile)

    def _load(self, fh):
        # split the input file based on the lines that start with a date
        entries = re.split(r"(\d+/\d+/\d+\s+\d+:\d+:\d+)", fh.read())

        # now grab each event. That is the date, and the data after it
        for plan_event in zip(entries[1::2], entries[2::2], strict=False):
            vevent = self.calendar.add("vevent")
            pevent = Event(vevent, plan_event, self.verbose)
            vevent.add("uid").value = pevent.uid
            self.events.append(pevent)

            if self.date_threshold_delta:
                if vevent.rruleset:
                    # If this is a repeating event,
                    # and there is no valid date after the threshold date,
                    # remove the event.
                    if not vevent.rruleset.after(self.date_threshold):
                        self.calendar.remove(vevent)
                else:
                    # Check if the end date is after the threshold date.
                    vdtend = datetime.datetime.combine(vevent.dtend.value, datetime.time(0))
                    dtdiff = datetime.datetime.now() - vdtend
                    if dtdiff > self.date_threshold_delta:
                        # remove the event if it is too far in the past
                        self.calendar.remove(vevent)
            # put all the datetimes into the current timezone
            # even if the object has been removed from the calendar.
            if isinstance(vevent.dtstart.value, datetime.datetime):
                vevent.dtstart.value = vevent.dtstart.value.replace(tzinfo=self.timezone)
            if isinstance(vevent.dtend.value, datetime.datetime):
                vevent.dtend.value = vevent.dtend.value.replace(tzinfo=self.timezone)

    def save_plan(self, fh):
        for event in self.events:
            fh.write(event.plan)

    def save_single_events(self, outdir=None):
        """
        Save each event as it's own ICS file.
        """
        if not outdir:
            outdir = "/tmp"  # nosec
        timezone = pytz.timezone(tzlocal.get_localzone_name())
        for event in self.events:
            file = f"{outdir}/{event.uid}.ics"

            calendar = vobject.iCalendar()
            tz = calendar.add("vtimezone")
            tz.settzinfo(timezone)

            with open(file, mode="w+", encoding="utf8") as fh:
                vevent = calendar.add("vevent")
                vevent.copy(event.ics)
                fh.write(calendar.serialize().translate(translate_map))

    def pprint(self):
        return self.calendar.serialize().translate(translate_map)
