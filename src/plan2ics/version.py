"""
Hold the version number.

This file should be updated by bump2version.

"""

VERSION = "1.0.2"
