#!/usr/bin/env python
"""
netplan2ics.py - Convert a netplan file into an iCalendar file.

Information gathered from:
- the Perl script plan2vcs by Bert Bos
- the plan manpage (man -S4 plan)
- vobject readme file
- the dateutil website

Copyright (c) 2009, James Mitchell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# ruff: noqa: D101, D102, D103, S324, DTZ007, D107, S108
import argparse
import datetime

from .models import Dayplan
from .version import VERSION


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s " + VERSION,
        help=f"show the program's version and exit ({VERSION})",
    )
    parser.add_argument("infiles", nargs="+", help="list of calendars to process.")
    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        action="store_true",
        default=False,
        help="print info while running [default: %(default)s]",
    )
    parser.add_argument(
        "-w",
        "--weeks",
        dest="weeks",
        default=None,
        type=int,
        help="include events since X weeks in the past.",
    )
    parser.add_argument(
        "-s",
        "--save",
        dest="do_save",
        default=False,
        action="store_true",
        help="re-save the plan file after processing. [default: %(default)s]",
    )
    parser.add_argument(
        "-d",
        "--dir",
        dest="output_dir",
        default="",
        help="directory to save individual ICS files.",
    )

    opts = parser.parse_args()
    date_threshold_delta = None
    if opts.weeks:
        date_threshold_delta = datetime.timedelta(weeks=opts.weeks)

    for file in opts.infiles:
        with open(file, mode="rb") as fh:
            c = Dayplan(fh, date_threshold_delta, opts.verbose)

        if opts.output_dir:
            c.save_single_events(outdir=opts.output_dir)
        else:
            print(c.pprint())

        if opts.do_save:
            with open(file, mode="w+", encoding="utf8") as fh:
                c.save_plan(fh)


if __name__ == "__main__":
    main()
