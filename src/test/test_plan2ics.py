"""
Tests are run using pytest.
"""

import datetime
import re
from io import StringIO

import pytz
import tzlocal

from plan2ics import Dayplan

DEFAULT_TZ = pytz.timezone(tzlocal.get_localzone_name())


test_calendar = """
9/11/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 0 15 0
N    Monthly event - 1,2,3,LAST
9/11/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 257 0 0
N    Monthly event - first Sunday
10/5/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    259200 1286323200 0 0 0
N    Daily Event - every 3 days, end 2010
9/11/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 8450 2 0
N    Monthly event - first and last Monday, and 1st of the month
9/11/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 0 0 1
E    9/11/2010
N    Yearly event
M    This is the text
M    of my
M    YEARLY EVENT
1/1/2001  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 0 0 1
N    New Year's Day
S    #Pilot: 1 J_Mitchell_1349521844 6d314cd8fbc3e89b33abdf330988e979 0 1323214
"""


def test_loading():
    """
    Test loading a calendar.
    """
    fhandle = StringIO(test_calendar)
    p = Dayplan(fhandle)
    events = [e for e in p.calendar.getChildren()]
    assert len(events) == 7


def test_yearly():
    """
    Test we can identify a Yearly repeating event.
    """
    plan = """
9/11/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 0 0 1
E    9/11/2010
N    Yearly event
M    This is the text
M    of my
M    YEARLY EVENT
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.rrule.value == "FREQ=YEARLY"


def test_exception():
    """
    Test handling an exception.
    """
    plan = """
9/11/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 0 0 1
E    9/11/2010
N    Yearly event
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.exdate.value == [datetime.date(2010, 9, 11)]


def test_until():
    """
    Test handling a repeating event with an end date.
    """
    plan = """
9/11/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 1286323200 0 0 1
E    9/11/2010
N    Yearly event
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert re.search("UNTIL=20101006", p.calendar.vevent.rrule.value)


def test_daily():
    """
    Test handling a repeating daily event.
    """
    plan = """
10/5/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    259200 1286323200 0 0 0
N    Daily Event - every 3 days, end 2010
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.rrule.value == ("FREQ=DAILY;INTERVAL=3;UNTIL=20101006")


def test_monthly1():
    """
    Test handling a repeating monthly event.
    """
    plan = """
9/11/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 0 15 0
N    Monthly event - 1,2,3,LAST
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.rrule.value == "FREQ=MONTHLY;BYMONTHDAY=1,2,3,-1"


def test_monthly2():
    """
    Test handling a repeating monthly event.
    """
    plan = """
9/11/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 8963 0 0
N    Monthly event - first, second and last Sunday and Monday
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.rrule.value == ("FREQ=MONTHLY;BYSETPOS=1,2,-1;BYDAY=MO,SU")


def test_weekly():
    """
    Test handling a repeating weekly event.
    """
    plan = """
7/21/2009  16:0:0  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 4 0 0
N    Weekly event. Tuesday at 4pm
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.rrule.value == "FREQ=WEEKLY;BYDAY=TU"


def test_dtend():
    """
    Test handling an event.
    """
    plan = """
7/21/2009  16:0:0  0:0:0  0:0:0  0:0:0  ---------- 0 0
N    Event at 4pm
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.dtstart.value == datetime.datetime(2009, 7, 21, 16, 0).astimezone(
        DEFAULT_TZ
    )
    assert p.calendar.vevent.dtend.value == datetime.datetime(2009, 7, 21, 16, 0).astimezone(
        DEFAULT_TZ
    )


def test_duration():
    """
    Test handling an event duration.
    """
    plan = """
7/21/2009  16:0:0  1:30:0  0:0:0  0:0:0  ---------- 0 0
N    Event at 4pm for 1hr 30min
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.dtstart.value == datetime.datetime(2009, 7, 21, 16, 0).astimezone(
        DEFAULT_TZ
    )
    assert p.calendar.vevent.dtend.value == datetime.datetime(2009, 7, 21, 17, 30).astimezone(
        DEFAULT_TZ
    )


def test_allday():
    """
    Test handling an all day event.
    """
    plan = """
7/21/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
N    All day event
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.dtstart.value == datetime.date(2009, 7, 21)
    assert p.calendar.vevent.dtend.value == datetime.date(2009, 7, 22)


def test_yearly_allday():
    """
    Test handling an all day event, repeating yearly.
    """
    plan = """
7/21/2009  99:99:99  0:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 0 0 0 1
N    All day event, repeat yearly
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.dtstart.value == datetime.date(2009, 7, 21)
    assert p.calendar.vevent.dtend.value == datetime.date(2009, 7, 22)
    assert p.calendar.vevent.rrule.value == "FREQ=YEARLY"


def test_duration_and_until():
    """
    Test handling an event with duration and end date.
    """
    plan = """
2/14/2009  8:30:0  2:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 1238198400 64 0 0
N    Weekly event on Saturday, duration is 2hrs, until 2009/03/28
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    assert p.calendar.vevent.dtstart.value == datetime.datetime(2009, 2, 14, 8, 30).astimezone(
        DEFAULT_TZ
    )
    assert p.calendar.vevent.dtend.value == datetime.datetime(2009, 2, 14, 10, 30).astimezone(
        DEFAULT_TZ
    )
    assert re.search("UNTIL=20090328", p.calendar.vevent.rrule.value)


def test_ensure_unicode():
    """
    Test the output is unicode.
    """
    # because the Ligthning/Sunbird calendar objects otherwise
    plan = """
2/14/2009  8:30:0  2:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 1238198400 64 0 0
N    Weekly event on Saturday, duration is 2hrs, until 2009/03/28
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    o = p.pprint()
    assert isinstance(o, str)


def test_translate_chars():
    """
    Test translating bad characters.
    """
    plan = """
2/14/2009  8:30:0  2:0:0  0:0:0  0:0:0  ---------- 0 0
R    0 1238198400 64 0 0
N    this event has an 0xa0 char that kills unicode \xa0
    """
    fhandle = StringIO(plan)
    p = Dayplan(fhandle)
    print(p.pprint())
    o = p.pprint()
    assert isinstance(o, str)
